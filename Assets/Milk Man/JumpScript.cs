using UnityEngine;
using System.Collections;

public class JumpScript : MonoBehaviour
{

    public int baseSpeed;
    public int sprint;
    private int currentSpeed;
    public bool facingRight;

    public float jumpHeight;

    public bool isGrounded;

    // Update is called once per frame
    void Update()
    {
        Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
        transform.position += move * currentSpeed * Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpHeight), ForceMode2D.Force);
        }
        
        if (Input.GetKey(KeyCode.LeftShift))
        {
            currentSpeed = baseSpeed + sprint;
        }
        else
        {
            currentSpeed = baseSpeed;
        }
    }
 
    void OnTriggerEnter2D()
    {
        isGrounded = true;
    }
    void OnTriggerStay2D()
    {
        isGrounded = true;
    }
    void OnTriggerExit2D()
    {
        isGrounded = false;
    }
}